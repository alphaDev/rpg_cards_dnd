import math

from PIL import Image, ImageDraw, ImageFont
from PIL.Image import Resampling

from common import save_card, load_font, load_card_info, draw_left_text, draw_center_text, Box, auto_split_text

title_font = None
sub_title_font = None
big_text_font = None
small_text_font = None
bottom_big_text_font = None
capa_text_font = None

pos_title = (44, 8)
sub_pos_title = (44, 70)
type_pos = (662, 70)
power_box = Box(669, 4, 69, 67)
str_stat_box = Box(72, 105, 74, 74)
str_mod_box = Box(85, 173, 49, 28)
dex_stat_box = Box(72, 215, 74, 74)
dex_mod_box = Box(85, 283, 49, 28)
con_stat_box = Box(72, 325, 74, 74)
con_mod_box = Box(85, 393, 49, 28)

int_stat_box = Box(598, 105, 74, 74)
int_mod_box = Box(610, 173, 49, 28)
wis_stat_box = Box(598, 215, 74, 74)
wis_mod_box = Box(610, 283, 49, 28)
cha_stat_box = Box(598, 325, 74, 74)
cha_mod_box = Box(610, 393, 49, 28)

align_box = Box(179, 981, 65, 45)
size_box = Box(299, 981, 65, 45)
speed_box = Box(417, 981, 65, 45)
ca_box = Box(535, 981, 65, 45)
pv_box = Box(653, 981, 65, 45)
perception_box = Box(60, 981, 65, 45)

pos_art = (220, 100)
size_art = (320, 320)

capa_text_box = Box(8, 434, 728, 535)


def load_common():
    global title_font, sub_title_font, big_text_font, small_text_font, bottom_big_text_font, capa_text_font

    title_font = load_font('arial', 59)
    sub_title_font = load_font('arial', 26)
    big_text_font = load_font('arial', 48)
    bottom_big_text_font = load_font('arial', 40)
    small_text_font = load_font('arial', 30)
    capa_text_font = ImageFont.truetype('img_template_mob/Plantin-Light.otf', 28)


def parse_stats(stats):
    mods = []
    _stats = []
    for i, stat in enumerate(stats.split()):
        _stats.append(stat)
        if not stat or stat == '-':
            mods.append('-')
        else:
            stat = int(stat)
            mod = math.floor(((stat - 10) / 2))
            if mod >= 0:
                mod = '+%s' % mod
            else:
                mod = str(mod)

            mods.append(mod)

    return _stats, mods


def build_card(card_info):
    print(card_info)
    with Image.open('img_template_mob/dnd_card mob template.png') as im:
        draw = ImageDraw.Draw(im)

        name_size = card_info.get('name_size', None)
        if name_size:
            title_font_cs_size = load_font('arial', name_size)
            draw.multiline_text(pos_title, card_info['name'], (0, 0, 0), font=title_font_cs_size)
        else:
            draw.multiline_text(pos_title, card_info['name'], (0, 0, 0), font=title_font)
        draw.multiline_text(sub_pos_title, card_info['sub_name'], (80, 80, 80), font=sub_title_font)
        draw_left_text(draw, card_info['type'], sub_title_font, type_pos[0], type_pos[1])
        draw_center_text(draw, card_info['power'], big_text_font, power_box)

        stats, mods = parse_stats(card_info['stats'])
        draw_center_text(draw, stats[0], big_text_font, str_stat_box)
        draw_center_text(draw, mods[0], small_text_font, str_mod_box)
        draw_center_text(draw, stats[1], big_text_font, dex_stat_box)
        draw_center_text(draw, mods[1], small_text_font, dex_mod_box)
        draw_center_text(draw, stats[2], big_text_font, con_stat_box)
        draw_center_text(draw, mods[2], small_text_font, con_mod_box)
        draw_center_text(draw, stats[3], big_text_font, int_stat_box)
        draw_center_text(draw, mods[3], small_text_font, int_mod_box)
        draw_center_text(draw, stats[4], big_text_font, wis_stat_box)
        draw_center_text(draw, mods[4], small_text_font, wis_mod_box)
        draw_center_text(draw, stats[5], big_text_font, cha_stat_box)
        draw_center_text(draw, mods[5], small_text_font, cha_mod_box)

        draw_center_text(draw, card_info['align'], bottom_big_text_font, align_box)
        draw_center_text(draw, card_info['size'], bottom_big_text_font, size_box)
        draw_center_text(draw, card_info['speed'], bottom_big_text_font, speed_box)
        draw_center_text(draw, card_info['CA'], bottom_big_text_font, ca_box)
        draw_center_text(draw, card_info['PV'], bottom_big_text_font, pv_box)
        draw_center_text(draw, card_info['perception'], bottom_big_text_font, perception_box)

        with Image.open(card_info['art']) as im_art:
            im_art = im_art.resize(size_art, Resampling.BICUBIC)
            im.paste(im_art, pos_art, im_art)

        _capa_text_font = capa_text_font
        capa_text_font_size = card_info.get('capa_text_font_size', None)
        if capa_text_font_size:
            _capa_text_font = ImageFont.truetype('img_template_mob/Plantin-Light.otf', capa_text_font_size)
        reactions = card_info.get('reactions', None)
        text = auto_split_text(capa_text_box.width, ('%s\n%s\n%s' % (card_info['capa'], card_info['actions'], reactions) if reactions else '%s\n%s' % (card_info['capa'], card_info['actions'])), _capa_text_font, draw)
        draw.multiline_text((capa_text_box.x, capa_text_box.y), text, (0, 0, 0), font=_capa_text_font)

        save_card(im, card_info, 'mob')


def main():
    load_common()
    for card_info in load_card_info('cards_info_mob.yml'):
        build_card(card_info)


if __name__ == '__main__':
    main()
