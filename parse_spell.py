import re

from bs4 import BeautifulSoup
from prettytable import PrettyTable

with open(r"C:\perso\onedrive\jdr\dnd\all_spells\_raw_dumps\Spells » Dungeons & Dragons - D&D 5e.html", 'r', encoding='utf8') as f:
    base_file = f.read()
soup = BeautifulSoup(base_file, features='html.parser')

x = PrettyTable()
x.field_names = ['name', 'name_en', 'name_fr', 'lvl', 'school', 'ritual', 'casting_time', 'range', 'components', 'duration', 'concentration', 'resume', 'description', 'upper_lvl', 'classes', 'tcoe_classes', 'source']

all_div = list(soup.body.contents[2])
for div in all_div:
    current = div.h1
    name = current.get_text()
    name = name.replace('/', '-')

    current = current.next_sibling
    names = current.get_text()
    m = re.match(r'\[ ([^\[\]]*) ]( \[ ([^\[\]]*) ])?', names)
    gs = m.groups()
    name_en = gs[0]
    name_fr = '' if gs[1] is None else gs[2]

    current = current.next_sibling
    lvl_and_school = current.get_text()
    m = re.match(r'niveau ([0-9]) - ([^() ]*)( \(rituel\))?', lvl_and_school)
    gs = m.groups()
    lvl = gs[0]
    school = gs[1]
    ritual = gs[2] is not None

    current = current.next_sibling
    casting_time = current.get_text()
    casting_time = casting_time.replace('Temps d\'incantation : ', '')

    current = current.next_sibling
    _range = current.get_text()
    _range = _range.replace('Portée : ', '')

    m = re.search(r'([0-9,]*) mètres', _range)
    if m:
        gs = m.groups()
        m_range = gs[0]
        c_range = int(float(m_range.replace(',', '.')) / 1.5)
        new_range = '%sc %sm' % (c_range, m_range)
        _range = re.sub(r'([0-9,]* mètres)', new_range, _range)

    current = current.next_sibling
    components = current.get_text()
    components = components.replace('Composantes : ', '')

    current = current.next_sibling
    duration = current.get_text()
    duration = duration.replace('Durée : ', '')
    concentration = duration.startswith('concentration')
    duration = duration.replace('concentration, ', '')

    current = current.next_sibling
    resume = u''
    if current['class'] == ['resume']:
        resume = current.get_text()
        current = current.next_sibling
    description = current.get_text()

    upper_lvl = ''

    classes = []
    current = current.next_sibling
    while current['class'] == ['classe']:
        classe = current.get_text()
        classes.append(classe)
        current = current.next_sibling

    tcoe_classes = []
    while current['class'] == ['tcoe']:
        classe = current.get_text()
        tcoe_classes.append(classe)
        current = current.next_sibling

    tcoe_classes = [i.replace(' [TCoE]', '') for i in tcoe_classes]
    source = current.get_text()

    # print(f'name:{name} name_en:{name_en} lvl_and_school:{lvl_and_school}')
    x.add_row([name, name_en, name_fr, lvl, school, ritual, casting_time, _range, components, duration, concentration, resume, description, upper_lvl, classes, tcoe_classes, source])
    # print(div.prettify())
    # break

print(x)

with open(r'C:\perso\onedrive\jdr\dnd\all_spells\all spell.csv', 'wt', encoding='utf8', newline='') as f:
    f.write(x.get_formatted_string('csv'))
with open(r'C:\perso\onedrive\jdr\dnd\all_spells\all spell.json', 'wt', encoding='utf8', newline='') as f:
    f.write(x.get_formatted_string('json', header=False))
