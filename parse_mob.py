import json
import re
import sys

from bs4 import BeautifulSoup, NavigableString, Tag, PageElement
from prettytable import PrettyTable


FILTER = (
    # day
    'Roturier',
    'Éclaireur',
    'Bandit',
    'Nuée de corbeaux',
    'Corbeau-garou',
    'Loup sanguinaire',
    'Loup',
    'Berserker',
    'Loup-garou',
    'Druide',
    'Arbuste éveillé',
    'Résinieux',
    'Épouvantail',
    'Revenant',
    # night
    'Fantôme',
    'Nuée de chauves-souris',
    'Zombi',
    'Zombi de Strahd',
    'Feu follet',
)
# FILTER = None

INVOCATION_NAMES = (
    'Incantation innée',
    'Incantation innée (psioniques)',
    'Incantation innée (psionniques)',
    'Incantation',
    'Incantation (forme d\'homme-lézard uniquement)',
    'Incantation innée (forme d\'abomination uniquement)',
    'Incantation innée (forme de yuan-ti uniquement)',
    'Incantation (forme de yuan-ti uniquement)',
    'Incantation (forme humanoïde uniquement)',
    'Incantation (guérisseur)',
    'Incantation (mage)',
    'Amulette du rêveur',
    'Incantation innée (forme d\'anathème uniquement)',
)


def load_raw():
    print('start loading raw dump')
    with open(r'C:\perso\onedrive\jdr\dnd\all_monsters\_raw_dump\Monsters » Dungeons & Dragons - D&D 5e - full.html', 'r', encoding='utf8') as f:
    # with open(r'C:\perso\onedrive\jdr\dnd\all_monsters\_raw_dump\Monsters » Dungeons & Dragons - D&D 5e - working.html', 'r', encoding='utf8') as f:
    # with open(r'C:\perso\onedrive\jdr\dnd\all_monsters\_raw_dump\Monsters » Dungeons & Dragons - D&D 5e - SDR.html', 'r', encoding='utf8') as f:
        base_file = f.read()
    print('raw dump read')
    soup = BeautifulSoup(base_file, features='html.parser')
    print('raw dump load')
    return soup


def parse_one_bloc(div):
    # print(f'-' * 20)
    
    data = {}
    
    current = div.h1
    current: NavigableString | Tag | PageElement | None
    name = current.get_text()
    name = name.replace('/', '-')
    # print(f'name :{name}')
    data['name'] = name
    
    current = current.next_sibling
    name_vo = current.get_text()
    m = re.match(r'\[ ([^\[\]]*) ]( \[ ([^\[\]]*) ])?', name_vo)
    gs = m.groups()
    name_en = gs[0]
    name_fr = '' if gs[1] is None else gs[2]
    # print(f'name_en :{name_en}')
    # print(f'name_fr :{name_fr}')
    data['name_en'] = name_en
    data['name_fr'] = name_fr
    
    current = current.next_sibling
    current = current.contents[0]
    size_type_align = current.get_text()
    # #print(f'size_type_align :{size_type_align}')
    m = re.match(r'(?P<type>[^(]+)(\((?P<sub_type>.*)\))?(.*)? de taille (?P<size>.+), (?P<align>.+)', size_type_align)
    mob_type = m.group('type')
    sub_type = m.group('sub_type')
    size = m.group('size')
    align = m.group('align')
    # print(f'mob_type :{mob_type}')
    # print(f'sub_type :{sub_type}')
    # print(f'size :{size}')
    # print(f'align :{align}')
    data['mob_type'] = mob_type
    data['sub_type'] = sub_type
    data['size'] = size
    data['align'] = align
    
    current = current.next_sibling
    # separator
    yellow_bloc = current.next_sibling
    yellow_bloc: NavigableString | Tag | PageElement | None
    current = yellow_bloc.contents[0]
    # ca text
    current = current.next_sibling
    ca = current.get_text()
    current = current.next_sibling
    while current.name != 'br':
        ca += current.get_text()
        current = current.next_sibling
    m = re.match(r' (?P<ca>[0-9]+)( \((?P<ca_type>[^(]+)\))?', ca)
    ca = m.group('ca')
    ca_type = m.group('ca_type') or ''
    # print(f'ca :{ca}')
    # print(f'ca_type :{ca_type}')
    data['ca'] = ca
    data['ca_type'] = ca_type
    
    # live text
    current = current.next_sibling
    current = current.next_sibling
    live_raw = current.get_text().strip()
    m = re.match(r'([0-9]+) \(([0-9d+\- ]+)\)', live_raw)
    live = ''
    dice_live = ''
    if m:
        live = m[1]
        dice_live = m[2]
    data['raw_live'] = live_raw
    data['live'] = live.strip()
    data['dice_live'] = dice_live.strip()
    
    # print(f'live :{live}')
    
    current = current.next_sibling
    # move text
    current = current.next_sibling
    current = current.next_sibling
    move = current.get_text()
    new_move = []
    move = re.sub(r'([0-9]+),([0-9]+)', r'\1@\2', move)
    for move_type in move.split(','):
        move_type = move_type.strip()
        move_type = move_type.replace('@', ',')
        m = re.search(r'([0-9,O]*) m', move_type)
        # #print(f'move_type :{move_type}')
        if m:
            gs = m.groups()
            # #print(f'gs :{gs}')
            m_move = gs[0]
            m_move = m_move.replace('O', '0')
            c_move = int(float(m_move.replace(',', '.')) / 1.5)
            move_as_c = '%sc %sm' % (c_move, m_move)
            move_type = re.sub(r'([0-9,]*) m', move_as_c, move_type)
        else:
            m = re.search(r'([0-9,O]*) ft.', move_type)
            if m:
                move_type = move_type.replace('climb', 'escalade')
                gs = m.groups()
                # #print(f'gs :{gs}')
                m_move = gs[0]
                m_move = m_move.replace('O', '0')
                move_as_feet = int(float(m_move.replace(',', '.')))
                move_as_meter = round((move_as_feet * 1.5 * 20 / 5)) / 20
                move_as_case = int(move_as_meter / 1.5)
                move_as_c = '%sc %sm' % (move_as_case, move_as_meter)
                move_type = re.sub(r'([0-9,]*) ft.', move_as_c, move_type)
            
            else:
                print(f'name :{name}')
                print(f'move :{move}')
                print(f'move_type :{move_type}')
        new_move.append(move_type)
    new_move = ','.join(new_move)
    # print(f'new_move :{new_move}')
    data['move'] = new_move
    
    # separator
    current = current.next_sibling
    current = current.next_sibling
    stats = []
    mod_stats = []
    for _ in range(6):
        stat = current.get_text()
        m = re.match(r'\w{3}(?P<stat>[0-9]+) \((?P<mod_stat>[0-9+-]+)\)', stat)
        stat = m.group('stat')
        mod_stat = m.group('mod_stat')
        stats.append(stat)
        mod_stats.append(mod_stat)
        current = current.next_sibling
    
    data['stats_for'] = stats[0]
    data['mod_stats_for'] = mod_stats[0]
    data['stats_dex'] = stats[1]
    data['mod_stats_dex'] = mod_stats[1]
    data['stats_con'] = stats[2]
    data['mod_stats_con'] = mod_stats[2]
    data['stats_int'] = stats[3]
    data['mod_stats_int'] = mod_stats[3]
    data['stats_sag'] = stats[4]
    data['mod_stats_sag'] = mod_stats[4]
    data['stats_cha'] = stats[5]
    data['mod_stats_cha'] = mod_stats[5]
    
    stats = ','.join(stats)
    mod_stats = ','.join(mod_stats)
    # print(f'stats :{stats}')
    # print(f'mod_stats :{mod_stats}')
    data['stats'] = stats
    data['mod_stats'] = mod_stats
    
    # separator
    current = current.next_sibling
    
    mastery = ''
    prev_current = current
    while True:
        current, saving_throw = parse_saving_throw(current)
        current, competences = parse_competences(current)
        current, dgm_vulnerability = parse_dgm_vulnerability(current)
        current, dgm_immunity = parse_dgm_immunity(current)
        current, dgm_resistance = parse_dgm_resistance(current)
        current, states_immunity = parse_states_immunity(current)
        current, sens = parse_sens(current)
        current, languages = parse_langues(current)
        current, power = parse_power(current)
        if current:
            current, mastery = parse_mastery(current)
        
        if power:
            break
        
        if prev_current == current:
            # print(f'current:{current.get_text()}')
            # print('!!!!!!!!!! Missing data !!!!!!!!!!!!!!')
            sys.exit()
        prev_current = current
    
    if current is not None:
        # print(f'current:{current.get_text()}')
        # print('!!!!!!!!!! Missing data !!!!!!!!!!!!!!')
        sys.exit()
    
    data['saving_throw'] = saving_throw.strip()
    data['competences'] = competences.strip()
    data['sens'] = sens.strip()
    data['dgm_vulnerability'] = dgm_vulnerability.strip()
    data['dgm_resistance'] = dgm_resistance.strip()
    data['dgm_immunity'] = dgm_immunity.strip()
    data['states_immunity'] = states_immunity.strip()
    data['languages'] = languages.strip()
    
    power_strip = power.strip()
    m = re.match(r'([0-9/]+) \(([0-9]+) PX\)', power_strip)
    if power_strip == '-':
        cr = '-'
        xp = '-'
    elif m:
        cr = m[1]
        xp = m[2]
    else:
        cr = '-'
        xp = '-'
        print(f'name :{name}')
        print(f'power_strip :{power_strip}')
    
    data['cr'] = cr
    data['xp'] = xp
    data['mastery'] = mastery.strip()
    
    current = yellow_bloc.next_sibling
    # separator
    current = current.next_sibling
    capacities = []
    data['capacities'] = capacities
    while current and not (hasattr(current, 'get') and current.get('class', '') == ['rub']):
        data_capacity = {}
        capacities.append(data_capacity)
        if hasattr(current, 'contents'):
            capacity = current.contents
            capacity_name = capacity[0].get_text()
            # print(f'capacity -> name:{capacity_name}')
            data_capacity['name'] = capacity_name
            
            capacity_text = capacity[1].get_text()
            if capacity_text.startswith('. '):
                capacity_text = capacity_text[2:]
            
            if capacity_name in INVOCATION_NAMES:
                current = parse_spell_casting(current, data_capacity)
            
            if capacity_name == 'Type de malison':
                current = current.next_sibling
                capacity_bullet = []
                while current.name != 'p' and current.name != 'div':
                    entry = current.get_text()
                    entry = f'* {entry}'
                    capacity_bullet.append(entry)
                    # print(f'---> entry:{entry}')
                    current = current.next_sibling
                    if current.name == 'br':
                        current = current.next_sibling
                capacity_text += '\n'.join(capacity_bullet)
                current = current.previous_sibling
            
            if capacity_name == 'Rage de Vaprak (Recharge après un repos court ou long)':
                current = current.next_sibling
                entry = current.get_text()
                capacity_text += f'\n{entry}'
                for _ in range(4):
                    current = current.next_sibling
                    entry = current.get_text()
                    capacity_text += f'\n* {entry}'
                current = current.next_sibling
            
            if capacity_name == 'Geule de la démence':
                for _ in range(3):
                    current = current.next_sibling
                    entry = current.get_text()
                    capacity_text += f'\n* {entry}'
                current = current.next_sibling
            
            # print(f'         -> text:{capacity_text}')
            data_capacity['text'] = capacity_text
        else:
            data_capacity['name'] = str(current)
            # print(f'capacity -> name:{data_capacity["name"]}')
            current = current.next_sibling
        
        current = current.next_sibling
    
    # print('=' * 5)
    
    while current and current.get_text() == 'Actions':
        current = parse_action(current, data, 'actions')
    
    if current and current.get_text() == 'Actions légendaires':
        current = current.next_sibling
        legendary_actions_desc = current.get_text()
        if 'actions légendaires, en choisissant parmi les options suivantes.' in legendary_actions_desc:
            # print(f'legendary_actions_desc:{legendary_actions_desc}')
            data['legendary_actions_desc'] = legendary_actions_desc
        else:
            current = current.previous_sibling
        current = parse_action(current, data, 'legendary_actions')
    
    if current and current.get_text() == 'Actions bonus':
        current = parse_action(current, data, 'actions_bonus')
    
    if current and current.get_text() == 'Réactions':
        current = parse_action(current, data, 'reactions')
    
    if current and current.get_text() == 'Actions légendaires':
        current = current.next_sibling
        legendary_actions_desc = current.get_text()
        if 'actions légendaires, en choisissant parmi les options suivantes.' in legendary_actions_desc:
            # print(f'legendary_actions_desc:{legendary_actions_desc}')
            data['legendary_actions_desc'] = legendary_actions_desc
        else:
            current = current.previous_sibling
        current = parse_action(current, data, 'legendary_actions')
    
    if current and current.get_text() == 'Actions mythiques':
        current = parse_action(current, data, 'mythiques_actions')
    
    data['OGL'] = True
    if current and current.get_text() == 'Le bloc de stat complet de cette créature n\'est pas disponible (non OGL).':
        data['OGL'] = False
        current = current.next_sibling
    # print(f'OGL:{data["OGL"]}')
    
    if current:
        # print(f'current:{current.get_text()}')
        # print('!!!!!!!!!! Missing data !!!!!!!!!!!!!!')
        sys.exit()
    
    description = div.find(class_='description')
    if description:
        description = description.get_text()
        # print(f'description:{description}')
        data['description'] = description
    
    source = div.find(class_='source')
    if source:
        source = source.get_text()
        # print(f'source:{source}')
        data['source'] = source
    
    # #print(f'div:{div.contents}')
    
    data['type_sub_type_size_align'] = f'{data["sub_type"]} - {data["size"]} - {data["align"]}'
    
    return data


def parse_action(current, data, action_type):
    current = current.next_sibling
    
    if current and current.get_text().startswith('Pour le Type'):
        action_type += current.get_text()
        current = current.next_sibling
        if current and current.name == 'br':
            current = current.next_sibling
    
    actions = []
    data[action_type] = actions
    while current and not (hasattr(current, 'get') and current.get('class', [])):
        current: NavigableString | Tag | PageElement | None
        data_action = {}
        actions.append(data_action)
        action = current.get_text()
        if '.' in action:
            action_name, action_text = action.split('.', 1)
        else:
            action_name = action
            action_text = ''
        # print(f'{action_type} -> name:{action_name}')
        data_action['name'] = action_name
        current = current.next_sibling
        
        if action_name in INVOCATION_NAMES:
            # print(f'{len(action_type) * " "} -> text:{action_text}')
            data_action['action_text'] = action_text
            if current and current.name == 'br':
                current = current.next_sibling
            if ':' in current:
                current = current.previous_sibling
                current = parse_spell_casting(current, data_action)
        else:
            if current and current.name is None:
                action_text += current.get_text()
                current = current.next_sibling
            # print(f'{len(action_type) * " "} -> text:{action_text}')
            data_action['action_text'] = action_text
            if current and current.name == 'br':
                current = current.next_sibling
    
    return current


def parse_spell_casting(current, data_capacity):
    data_capacity['is_casting'] = True
    current = current.next_sibling
    casts = []
    data_capacity['casts'] = casts
    while current and current.name != 'p' and current.name != 'div':
        cast = {}
        casts.append(cast)
        cast_slot = current.get_text()
        # print(f'---> cast_slot:{cast_slot}')
        cast['cast_slot'] = cast_slot
        current = current.next_sibling
        spell = current.get_text()
        current = current.next_sibling
        if current and current.get_text() == ' (personnel uniquement)':
            spell += current.get_text()
            current = current.next_sibling
        cast['spell'] = spell
        # print(f'------> spell:{spell}')
        if current.name == 'br':
            current = current.next_sibling
    if current:
        current = current.previous_sibling
    return current


def parse_power(current):
    power = ''
    if 'Puissance' == current.get_text():
        current = current.next_sibling
        power = current.get_text()
        # print(f'Puissance :{power}')
        current = current.next_sibling
    return current, power


def parse_mastery(current):
    mastery = ''
    if 'Bonus de maîtrise' == current.get_text():
        current = current.next_sibling
        mastery = current.get_text()
        # print(f'Mastery :{mastery}')
        current = current.next_sibling
    return current, mastery


def parse_langues(current):
    languages = ''
    if 'Langues' == current.get_text():
        current = current.next_sibling
        languages = current.get_text()
        # print(f'Langues :{languages}')
        current = current.next_sibling
        current = current.next_sibling
    return current, languages


def parse_sens(current):
    sens = ''
    if 'Sens' == current.get_text():
        current = current.next_sibling
        # print(f'current:[{current.get_text()}]')
        if current.get_text() == ' ' and not current.name:
            current = current.next_sibling
            current = current.next_sibling
        sens = current.get_text()
        # print(f'sens :{sens}')
        current = current.next_sibling
        current = current.next_sibling
    return current, sens


def parse_states_immunity(current):
    states_immunity = ''
    if 'Immunités aux états' == current.get_text():
        current = current.next_sibling
        states_immunity = current.get_text()
        # print(f'states_immunity :{states_immunity}')
        current = current.next_sibling
        current = current.next_sibling
    return current, states_immunity


def parse_dgm_resistance(current):
    dgm_resistance = ''
    if 'Résistances aux dégâts' == current.get_text():
        current = current.next_sibling
        dgm_resistance = current.get_text()
        while current.name != 'br':
            current = current.next_sibling
            dgm_resistance += current.get_text()
        # print(f'dgm_resistance :{dgm_resistance}')
        # current = current.next_sibling
        current = current.next_sibling
    return current, dgm_resistance


def parse_dgm_immunity(current):
    dgm_immunity = ''
    text = current.get_text()
    if text.startswith('Damage immunities'):
        text = text.replace('Damage immunities', '')
        dgm_immunity = current.get_text()
        # print(f'dgm_immunity :{dgm_immunity}')
        current = current.next_sibling
        current = current.next_sibling
    
    if 'Immunités aux dégâts' == text:
        current = current.next_sibling
        dgm_immunity = current.get_text()
        # print(f'dgm_immunity :{dgm_immunity}')
        current = current.next_sibling
        current = current.next_sibling
    return current, dgm_immunity


def parse_dgm_vulnerability(current):
    dgm_vulnerability = ''
    if 'Vulnérabilités aux dégâts' == current.get_text():
        current = current.next_sibling
        dgm_vulnerability = current.get_text()
        # print(f'dgm_vulnerability :{dgm_vulnerability}')
        current = current.next_sibling
        current = current.next_sibling
    return current, dgm_vulnerability


def parse_competences(current):
    competences = ''
    if 'Compétences' == current.get_text():
        current = current.next_sibling
        competences = current.get_text()
        # print(f'Compétences :{competences}')
        current = current.next_sibling
        current = current.next_sibling
    return current, competences


def parse_saving_throw(current):
    saving_throw = ''
    if 'Jets de sauvegarde' == current.get_text():
        current = current.next_sibling
        saving_throw = current.get_text()
        # print(f'Jets de sauvegarde :{saving_throw}')
        current = current.next_sibling
        current = current.next_sibling
    return current, saving_throw


def main():
    soup = load_raw()
    
    all_data = []
    for div in soup.body.find_all(class_="bloc"):
        one_mob_data = parse_one_bloc(div)
        if not FILTER or one_mob_data['name'] in FILTER:
            all_data.append(one_mob_data)
    
    pretty_table = PrettyTable()
    pretty_table.field_names = field_names = [
        'name',
        'name_vo',
        'mob_type',
        'sub_type',
        'size',
        'align',
        'ca',
        'ca_type',
        'live',
        'move',
        'stats',
        'mod_stats',
        'saving_throw',
        'competences',
        'sens',
        'dgm_vulnerability',
        'dgm_resistance',
        'dgm_immunity',
        'states_immunity',
        'languages',
        'power',
        'capacities',
        'actions',
        'legendary_actions_desc',
        'legendary_actions',
        'reactions',
        'description',
        'source',
        'cr',
        'type_sub_type_size_align',
    ]
    for d in all_data:
        if set(d.keys()) > set(field_names):
            print(f'missing key in field_names : {d.keys()} > {field_names}')
        row = [str(d.get(col_name, '')) for col_name in field_names]
        pretty_table.add_row(row)
    # #print(pretty_table)
    
    with open(r'C:\perso\onedrive\jdr\dnd - Curse of Strahd\DM Notes\Random encounter.csv', 'wt', encoding='utf8', newline='') as f:
        f.write(pretty_table.get_formatted_string('csv'))
    
    # with open(r'C:\perso\onedrive\jdr\dnd\all_monsters\all mob.json', 'wt', encoding='utf8', newline='') as f:
    #     json.dump(all_data, f, indent=4)


if __name__ == '__main__':
    main()
