import yaml
from PIL import Image, ImageFont, ImageDraw
from yaml import SafeLoader

from common import load_font, auto_split_text, load_card_info, save_card

template_info = {}


def load_template_info(file_name):
    global template_info
    with open(file_name, encoding='utf-8') as f:
        template_info = yaml.load(f, Loader=SafeLoader)

    cards_info = load_card_info(template_info['cards_info']['path'], template_info['cards_info'].get('format', 'yml'))
    template_info['cards_info']['cards_info_load'] = cards_info

    img = Image.open(template_info['img_template']['path'])
    template_info['img_template']['img'] = img
    template_info['img_template']['draw'] = ImageDraw.Draw(img)
    template_info['img_template']['width'] = img.width
    template_info['img_template']['height'] = img.height
    img.close()
    for text_info in template_info['text'].values():
        font_info = text_info['font']
        font_size = font_info.get('size', font_info.get('size_max', 10))
        font = load_font_with_font_info(font_info, font_size)
        font_info['font_load'] = font

    text_to_img = template_info.get('text_to_img', {})
    for text_to_img_bloc, text_to_img_bloc_mapping in text_to_img.items():
        for text_to_replace, img_path in text_to_img_bloc_mapping.items():
            text_to_img_bloc_mapping[text_to_replace] = Image.open(img_path)


def load_font_with_font_info(font_info, font_size):
    if 'file' in font_info:
        font = ImageFont.truetype(font_info['file'], font_size)
    else:
        font = load_font(font_info['name'], font_size)
    return font


def adjust_font(text, draw, font_info, box_info):
    font_size = font_info.get('size_max')
    current_font = font_info['font_load']
    split_text = auto_split_text(box_info['width'], text, current_font, draw)
    height = box_info['height']
    left, top, right, bottom = draw.textbbox((0, 50), split_text, font=current_font)
    line_height = bottom - top
    while line_height > height:
        font_size -= 1
        current_font = load_font_with_font_info(font_info, font_size)
        split_text = auto_split_text(box_info['width'], text, current_font, draw)
        left, top, right, bottom = draw.textbbox((0, 50), split_text, font=current_font)
        line_height = bottom - top

    return current_font, split_text


def draw_text_with_template_info(prop_name, card_info):
    draw = template_info['img_template']['draw']
    info = template_info['text'][prop_name].copy()
    text = card_info.get(prop_name, info.get('text', 'TEXT NOT FOUND'))
    font_load = info['font']['font_load']
    text_to_img = info.get('text_to_img', '')

    if isinstance(text, dict):
        _text = text['text']
        if '{' in _text:
            _text = _text.format(**card_info)
        if text.get('exec', False):
            _text = eval(_text)
        text = _text

    if '{' in text:
        text = text.format(**card_info)

    position = (0, 0)
    anchor = 'la'
    if 'box' in info:
        box_info = info['box']
        pos_x = box_info['x']
        pos_y = box_info['y']
        anchor = box_info.get('anchor', anchor)

        size_max = info['font'].get('size_max')
        if size_max:
            font_load, text = adjust_font(text, draw, info['font'], box_info)
        else:
            text = auto_split_text(box_info['width'], text, font_load, draw)

        if anchor[0] == 'm':
            pos_x = box_info['width'] / 2 + pos_x
        if anchor[1] == 'm':
            pos_y = box_info['height'] / 2 + pos_y
        position = (pos_x, pos_y)
    elif 'position' in info:
        pos_x = info['position']['x']
        pos_y = info['position']['y']

        if pos_x == 'center':
            pos_x = int(template_info['img_template']['width'] / 2)
            anchor = 'm' + anchor[1]
        if pos_y == 'center':
            pos_y = int(template_info['img_template']['height'] / 2)
            anchor = anchor[0] + 'm'
        position = (pos_x, pos_y)

    draw.multiline_text(position, text, fill=(0, 0, 0), font=font_load, anchor=anchor)


def build_card(card_info):
    print(card_info)
    img = Image.open(template_info['img_template']['path'])
    template_info['img_template']['img'] = img
    template_info['img_template']['draw'] = ImageDraw.Draw(img)

    for prop_name in template_info['text']:
        draw_text_with_template_info(prop_name, card_info)

    save_card(template_info['img_template']['img'], card_info, template_info['out_folder'])
    img.close()


def build_cards(template_path):
    load_template_info(template_path)
    for card_info in template_info['cards_info']['cards_info_load']:
        build_card(card_info)
