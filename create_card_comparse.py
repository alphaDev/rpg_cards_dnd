import math

from PIL import Image, ImageDraw
from PIL.Image import Resampling

from common import save_card, load_font, load_card_info, draw_center_text, Box, auto_split_text, draw_left_text, draw_text

title_font = None
sub_title_font = None
big_text_font = None
small_text_font = None
bottom_big_text_font = None
capa_text_font = None

pos_name = (152, 308)
lvl_and_class = (1068, 232)
size_and_race = (1068, 342)
str_stat_box = Box(124, 508, 150, 100)
dex_stat_box = Box(394, 508, 150, 100)
con_stat_box = Box(664, 508, 150, 100)
int_stat_box = Box(934, 508, 150, 100)
wis_stat_box = Box(1204, 508, 150, 100)
cha_stat_box = Box(1474, 508, 150, 100)

str_mod_box = Box(162, 630, 75, 60)
dex_mod_box = Box(432, 630, 75, 60)
con_mod_box = Box(702, 630, 75, 60)
int_mod_box = Box(972, 630, 75, 60)
wis_mod_box = Box(1242, 630, 75, 60)
cha_mod_box = Box(1512, 630, 75, 60)

AC_box = Box(129, 789, 114, 118)
init_box = Box(302, 753, 217, 206)
speed_box = Box(534, 753, 217, 206)
PV_box = Box(764, 753, 217, 206)

art_box = Box(1066, 838, 550, 550)
jds_box = Box(103, 1060, 400, 68)
perception_box = Box(589, 1021, 75, 75)
comp_box = Box(100, 1147, 415, 293)
lang_box = Box(601, 1165, 343, 276)

big_box = Box(132, 1533, 1455, 861)


def load_common():
    global title_font, sub_title_font, big_text_font, small_text_font, bottom_big_text_font, capa_text_font

    title_font = load_font('arial', 59)
    sub_title_font = load_font('arial', 26)
    bottom_big_text_font = load_font('arial', 50)
    big_text_font = load_font('arial', 80)
    small_text_font = load_font('arial', 40)
    capa_text_font = load_font('arial', 40)
    # capa_text_font = ImageFont.truetype('img_template_mob/Plantin-Light.otf', 28)


def parse_stats(stats):
    mods = []
    _stats = []
    for i, stat in enumerate(stats.split()):
        _stats.append(stat)
        if not stat or stat == '-':
            mods.append('-')
        else:
            stat = int(stat)
            mod = math.floor(((stat - 10) / 2))
            if mod >= 0:
                mod = '+%s' % mod
            else:
                mod = str(mod)

            mods.append(mod)

    return _stats, mods


def build_card(card_info):
    print(card_info)
    with Image.open('img_template_companions/companions.png') as im:
        draw = ImageDraw.Draw(im)

        draw_text(draw, card_info['name'], big_text_font, pos_name[0], pos_name[1], anchor='lb')
        draw_text(draw, '%s - %s' % (card_info['lvl'], card_info['class']), capa_text_font, lvl_and_class[0], lvl_and_class[1], anchor='lb')
        draw_text(draw, '%s - %s' % (card_info['size'], card_info['race']), capa_text_font, size_and_race[0], size_and_race[1], anchor='lb')

        stats, mods = parse_stats(card_info['stats'])
        draw_center_text(draw, stats[0], big_text_font, str_stat_box)
        draw_center_text(draw, mods[0], small_text_font, str_mod_box)
        draw_center_text(draw, stats[1], big_text_font, dex_stat_box)
        draw_center_text(draw, mods[1], small_text_font, dex_mod_box)
        draw_center_text(draw, stats[2], big_text_font, con_stat_box)
        draw_center_text(draw, mods[2], small_text_font, con_mod_box)
        draw_center_text(draw, stats[3], big_text_font, int_stat_box)
        draw_center_text(draw, mods[3], small_text_font, int_mod_box)
        draw_center_text(draw, stats[4], big_text_font, wis_stat_box)
        draw_center_text(draw, mods[4], small_text_font, wis_mod_box)
        draw_center_text(draw, stats[5], big_text_font, cha_stat_box)
        draw_center_text(draw, mods[5], small_text_font, cha_mod_box)

        draw_center_text(draw, card_info['AC'], big_text_font, AC_box)
        draw_center_text(draw, card_info['init'], big_text_font, init_box)
        draw_center_text(draw, card_info['speed'], load_font('arial', 65), speed_box)
        draw_center_text(draw, card_info['PV'], big_text_font, PV_box)

        draw_center_text(draw, card_info['perception'], load_font('arial', 60), perception_box)

        draw_center_text(draw, card_info['JdS'], capa_text_font, jds_box)

        text = auto_split_text(comp_box.width, card_info['comp'], capa_text_font, draw)
        draw.multiline_text((comp_box.x, comp_box.y), text, (0, 0, 0), font=capa_text_font)

        text = auto_split_text(lang_box.width, card_info['lang'], capa_text_font, draw)
        draw.multiline_text((lang_box.x, lang_box.y), text, (0, 0, 0), font=capa_text_font)

        text = auto_split_text(big_box.width, card_info['actions'], capa_text_font, draw)
        draw.multiline_text((big_box.x, big_box.y), text, (0, 0, 0), font=capa_text_font)

        with Image.open(card_info['art']) as im_art:
            im_art = im_art.resize((art_box.height, art_box.width), Resampling.BICUBIC)
            im.paste(im_art, (art_box.x, art_box.y), im_art)

        save_card(im, card_info, 'companions')


def main():
    load_common()
    for card_info in load_card_info('cards_info_companions.yml'):
        build_card(card_info)


if __name__ == '__main__':
    main()
