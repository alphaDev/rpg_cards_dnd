import json
import os.path
from collections import namedtuple

import yaml
from PIL import ImageFont
from matplotlib import font_manager
from yaml.loader import SafeLoader

Box = namedtuple('Box', ['x', 'y', 'width', 'height'])


def auto_split_text(expected_width, text, font, draw):
    new_text_lines = []
    current_line = []
    text = text.replace('\n', ' ___NEW_LINE___ ')
    for word in text.split():
        if word == '___NEW_LINE___':
            new_text_lines.append(' '.join(current_line))
            current_line = []
            continue
        current_line.append(word)
        text_line = ' '.join(current_line)
        left, top, right, bottom = draw.textbbox((0, 50), text_line, font=font)
        line_with = right - left
        if line_with > expected_width:
            new_text_lines.append(' '.join(current_line[:-1]))
            current_line = [word]

    if current_line:
        new_text_lines.append(' '.join(current_line))

    return '\n'.join(new_text_lines)


def draw_center_text(draw, text, font, box: Box, auto_split=False):
    if auto_split:
        text = auto_split_text(box.width, text, font, draw)
    left, top, right, bottom = draw.multiline_textbbox((3, 3), text, font=font)
    pos_x = (box.width - right + left) / 2 + box.x
    pos_y = box.height / 2 + box.y

    draw.multiline_text((pos_x, pos_y), text, (0, 0, 0), anchor='lm', font=font)


def draw_left_text(draw, text, font, left_bound, pos_y):
    left, top, right, bottom = draw.multiline_textbbox((0, pos_y), text, font=font)
    pos_x = left_bound - right + left
    draw.multiline_text((pos_x, pos_y), text, (0, 0, 0), font=font)


def draw_text(draw, text, font, pos_x, pos_y, anchor=None):
    draw.text((pos_x, pos_y), text, (0, 0, 0), font=font, anchor=anchor)


def load_font(font_name, font_size, weight=None):
    font_file_info = font_manager.FontProperties(family=font_name, weight=weight)
    file = font_manager.findfont(font_file_info)
    font = ImageFont.truetype(file, font_size)
    return font


def load_card_info(file_name, _format='yml'):
    with open(file_name, encoding='utf-8') as f:
        if _format == 'yml':
            return list(yaml.load_all(f, Loader=SafeLoader))
        elif _format == 'json':
            return json.load(f)


def save_card(im, card_info, folder_name):
    file_name = card_info.get('out', card_info['name'])
    if not os.path.exists(f'out/{folder_name}'):
        os.makedirs(f'out/{folder_name}')
    im.save(f'out/{folder_name}/{file_name}.png', 'PNG')
